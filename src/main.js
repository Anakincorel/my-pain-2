// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './vuex';

import AdrHeader from './components/header/AdrHeader';
import AdrSidebar from './components/sidebar/AdrSidebar';
import AdrButton from './components/ui/AdrButton';
import AdrModal from './components/ui/AdrModal';

Vue.config.productionTip = false;

Vue.component(AdrHeader.name, AdrHeader);
Vue.component(AdrSidebar.name, AdrSidebar);
Vue.component(AdrButton.name, AdrButton);
Vue.component(AdrModal.name, AdrModal);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
});
