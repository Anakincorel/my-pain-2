import posts from './posts';
import user from './user';
import modal from './modal'

const state = Object.assign(
  {
    error: {
      title: null,
      message: null,
      error: null
    }
  },
  posts,
  user,
  modal
);

export default state;
