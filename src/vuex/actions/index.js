import posts from './posts';
import user from './user';

const actions = Object.assign({}, posts, user);

export default actions;
