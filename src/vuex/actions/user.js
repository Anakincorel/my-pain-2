import * as type from '../types'
import { throwError } from '../../utils';

export default {
  [type.FETCH_USER]: ({ commit, getters }, { id }) => {
    const name = type.USER;
    commit(type.LOADING, name);
    return getters
      .GET('/users', { id })
      .then(model => {
        commit(type.SET_MODEL, { name, model: model[0] })
      })
      .catch(throwError(commit, 'Ошибка при получении данных пользователя'))
      .finally(() => commit(type.LOADED, name))
  }
}
